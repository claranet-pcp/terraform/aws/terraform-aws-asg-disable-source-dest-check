#!/usr/bin/python

import boto3
import botocore
from datetime import datetime
import os
import logging

# get logger
logger = logging.getLogger()
logger.setLevel(os.environ['LOG_LEVEL'])

# get current region, it is in ENV by default
aws_region = os.environ['AWS_DEFAULT_REGION']

ec2_resource = boto3.resource('ec2', region_name=aws_region)
asg_client = boto3.client('autoscaling', region_name=aws_region)


def lambda_handler(event, context):

    logger.debug('Event: {}'.format(event))

    if event["detail-type"] == "EC2 Instance-launch Lifecycle Action":
        instance_id = event["detail"]["EC2InstanceId"]
        logger.debug('Instance ID: {}'.format(instance_id))

        sd_disable(instance_id)
        logger.debug('SourceDestCheck Disabled')

        # complete lifecycle hook
        try:
            asg_client.complete_lifecycle_action(
                LifecycleHookName=event['detail']['LifecycleHookName'],
                AutoScalingGroupName=event['detail']['AutoScalingGroupName'],
                LifecycleActionToken=event['detail']['LifecycleActionToken'],
                LifecycleActionResult='CONTINUE'
            )
        except botocore.exceptions.ClientError as e:
            logger.error('Error completing life cycle hook for instance {}: \
                         {}'.format(instance_id, e.response['Error']['Code']))

def sd_disable(instance_id):

    instance = ec2_resource.Instance(instance_id)

    instance.modify_attribute(
        SourceDestCheck={
            'Value': False
        }
    )

    return True
