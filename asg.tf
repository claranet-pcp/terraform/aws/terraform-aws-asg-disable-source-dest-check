# ASG lifecycle hook
resource "aws_autoscaling_lifecycle_hook" "asg_hook" {
  name                   = "${var.envname}-${var.service}-sd-disable-hook"
  autoscaling_group_name = var.asg_name
  default_result         = "CONTINUE"
  heartbeat_timeout      = 60
  lifecycle_transition   = "autoscaling:EC2_INSTANCE_LAUNCHING"
}
