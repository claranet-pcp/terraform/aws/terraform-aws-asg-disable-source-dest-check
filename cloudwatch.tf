resource "aws_cloudwatch_event_rule" "sd_disable" {
  name        = var.cloudwatch_event_rule_name
  description = "Trigger for lambda Source/Dest check disable"

  event_pattern = <<PATTERN
{
  "detail-type": [
    "EC2 Instance-launch Lifecycle Action"
  ],
  "source": [
    "aws.autoscaling"
  ],
  "resources": [
    "${var.asg_arn}"
  ]
}
PATTERN
}

resource "aws_cloudwatch_event_target" "sd_disable" {
  rule = aws_cloudwatch_event_rule.sd_disable.name
  arn  = aws_lambda_function.sd_disable.arn
}

resource "aws_lambda_permission" "allow_cloudwatch_to_call_sd-disable" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.sd_disable.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.sd_disable.arn
}
