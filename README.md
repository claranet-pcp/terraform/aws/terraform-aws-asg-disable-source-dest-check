# terraform-aws-asg-disable-source-dest-check

This module will disable source/destination address checking on packets
to/from the instances in the specified ASG. This cannot be configured
in the launch configuration or ASG settings at the time of writing.

## Terraform Version Compatibility

Module Version|Terraform Version
---|---
v1.0.0|0.12.x
v0.1.0|0.11.x

## Usage

```
# disable Source/Dest Check
module "vpn_sd_disable" {
  source                     = "../modules/tf-aws-asg-sd-disable"
  envname                    = data.aws_region.current.name
  service                    = "vpn"
  lambda_function_name       = "vpn-sd-disable"
  cloudwatch_event_rule_name = "vpn-sd-disable"
  asg_arn                    = module.vpn_asg.asg_arn
  asg_name                   = module.vpn_asg.asg_name
  lambda_log_level           = "INFO"
}
```
